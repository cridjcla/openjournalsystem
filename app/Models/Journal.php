<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    use HasFactory;

    protected $fillable = [
        'subject',
        'title',
        'description',
        'issn',
        'online_issn',
        'cover_image',
        'archived_at',
        'archived_by',
        'published_at',
        'published_by'
    ];

    protected $hidden = [
        'archived_at',
        'archived_by',
        'published_by'
    ];

    public function scopeNotArchived() {
        return $this->where('archived_at', null);
    }

    public function scopePublished() {
        return $this->where('published_at', '!=', null)
                    ->where('archived_at', null);
    }
}
