<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', // author id
        'journal_id',
        'managing_editor',
        'title',
        'introduction',
        'abstract',
        'method',
        'result',
        'conclusion',
        'references',
        'recommendation',
        'acknowledgement',
        'published_at',
        'published_by',
        'archived_at',
        'archived_by',
        'status', // 0 = reviewing, 1 = approved, 2 = reject, 3 = on hold
        'peer_review_id',
        'keywords',
        'comment_summary',
        'total_resubmit',
        'file'
    ];

    public function allActive() {
        return $this->where('archived_at', null)
                    ->orderBy('created_at', 'desc');
    }

    public function articleAuthors() {
        return $this->hasMany(ArticleAuthor::class, 'article_id', 'id');
    }
}
