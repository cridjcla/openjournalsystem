<?php

namespace App\DTOs;

use App\Models\ArticleAuthor;
use App\Models\User;

class AuthorDto
{
    public string $fullname;
    public string $email;
    public string $affiliate;
    public string $address;

    public function __construct(string $fullname, string $email, string $affiliate, string $address)
    {
        $this->fullname = $fullname;
        $this->email = $email;
        $this->affiliate = $affiliate;
        $this->address = $address;
    }

    public static function fromUser(User $user): self 
    {
        $fullname = $user->first_name . ' ' . $user->last_name;

        return new self(
            $fullname,
            $user->email ?? '',
            $user->affiliate ?? '',
            $user->address ?? ''
        );
    }
    
    public static function fromArticleAuthor(ArticleAuthor $articleAuthor): self 
    {
        return new self(
            $articleAuthor->name,
            $articleAuthor->email ?? '',
            $articleAuthor->affiliate ?? '',
            $user->address ?? ''
        );
    }
}
