<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File; 

class FileService extends Model
{
    public function saveFile($req, $prop, $lastFilename = '', $additionalName = '', $mainFileName = '')
    {
        $file = $req->file($prop);
        $filename = null;

        if (!$file) {
            if (!empty($req->$prop)) return $req->$prop;
            return $filename;
        }

        if (!empty($lastFilename) && File::exists(public_path('files\\' . $lastFilename))) 
            File::delete(public_path('files\\' . $lastFilename));

        if (!empty($mainFileName)) $filename = $mainFileName;
        else $filename = rand(00000, 99999) . $additionalName . '.' . $file->getClientOriginalExtension();

        $file->move(public_path('files'), $filename);
        return $filename;
    }

    public function removeFile($filename, $path = 'files\\')
    {
        if (!empty($filename) && File::exists(public_path($path . $filename))) {
            return File::delete(public_path($path . $filename));
        }
    }
}
