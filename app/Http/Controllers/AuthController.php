<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function login(Request $req)
    {
        // validate $req values
        $req->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        if (Auth::attempt($req->only('email', 'password'))) {
            $user = Auth::user();
            $token = $user->createToken('myapptoken')->plainTextToken;

            return response()->json([
                'user' => $user,
                'token' => $token
            ]);
        }
        throw ValidationException::withMessages([
            'password' => ['Incorrect credentials']
        ]);
    }

    public function register(Request $req) 
    {
        $req->validate([
            'first_name' => 'required|string',
            'email' => 'required|string|unique:users',
            'password' => 'required|string|confirmed',
        ]);

        $inputs = $req->all();
        $inputs['password'] = bcrypt($req->password);

        $user = User::create($inputs);
        
        if ($user) {
            if (Auth::attempt($req->only('email', 'password'))) {
                $token = $user->createToken('myapptoken')->plainTextToken;

                // send email verification
                $this->sendEmailVerification($user->id, $inputs['email']);

                return response()->json([
                    'user' => $user,
                    'token' => $token
                ]);
            }
        }
        return response()->json(false);
    }

    public function sendEmailVerification($userId, $email) {
        $url = url('/') . '/#/email-verified?id='. $this->hasher($userId);
        $mailController = new MailController;
        if ($mailController->sendEmailVerification($email, $url)) {
            return response()->json('Email Verification Sent');
        }

        return response()->json('Email send error');
    }

    public function verifyEmail(Request $request) {
        $inputs = $request->all();

        if (empty($inputs['uId'])) {
            return response()->json('Failed');
        }

        $userId = $inputs['uId'];
        $user = User::findOrFail($this->dehasher($userId));
        $user->email_verified_at = new \DateTime();
        $user->save();
        return redirect()->to(env('REDIRECT_URI'));
    }

    public function forgotPassword(Request $request) {
        $email = $request->email;
        $mailer = new MailController;
        $hashed = $this->hasher($email);
        $url = env('CHANGE_PASSWORD_URI');
        $url .= '?code='.$hashed.'&email='. $email;
        $mailer->sendChangePasswordVerification($email, $url);
        return response()->json('Email verification sent');
    }

    private function hasher($text) {
        return openssl_encrypt($text, 'AES-128-CTR', 'changepass', 0, '1234567891011121');
    }

    private function dehasher($text) {
        return openssl_decrypt($text, 'AES-128-CTR', 'changepass', 0, '1234567891011121');
    }
}
