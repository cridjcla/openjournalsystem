<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPMailer\PHPMailer\PHPMailer;

class MailController extends Controller
{

  public function sendEmail(Request $request)
  {
    $mail = $this->getMailerInstance();
    $mail->addAddress($request->email);
    $mail->isHTML(true);

    $mail->Subject = $request->subject;
    $mail->Body = $request->body;
    if ($mail->send()) {
      return new Response('Email sent', 200);
    }
  }

  public function sendEmailUsingEmailMessage($email, $message, $subject = 'For you', $attachment = null, $isNoEmailCC = false)
  {
    $mail = $this->getMailerInstance();
    $mail->addAddress($email);
    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body = $message;
    
    if ($attachment != null) {
      try {
        $mail->addAttachment($attachment->getPathName(), $attachment->getClientOriginalName());
      } catch (\Throwable $th) {
        //throw $th;
      }
    }

    if ($mail->send()) {
      return new Response('Email sent', 200);
    }
  }

  public function sendEmailVerification($email, $url)
  {
    $mail = $this->getMailerInstance();
    $mail->addAddress($email);
    $mail->isHTML(true);

    $mail->Subject = 'Open Journal System email verification';
    $mail->Body = '<a href="' . $url . '">Click here to verify your email</a>';
    if ($mail->send()) {
      return true;
    }
    return false;
  }

  public function sendWelcomeMessage($email)
  {
    $mail = $this->getMailerInstance();
    $mail->addAddress($email);
    $mail->isHTML(true);

    $mail->Subject = 'Open Journal System';
    $mail->Body = '<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
        <div style="margin:50px auto;width:70%;padding:20px 0">
          <div style="border-bottom:1px solid #eee">
            <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Open Journal System</a>
          </div>
          <p style="font-size:1.1em">Dear Valued Client,</p>
          <p style="font-size:0.9em;">Regards,<br />Open Journal System</p>
          <hr style="border:none;border-top:1px solid #eee" />
          <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
            <p>Open Journal System</p>
          </div>
        </div>
      </div>';
    if ($mail->send()) {
      return true;
    }
    return false;
  }

  public function sendArticleReviewForApproval($articleUrl, $email)
  {
    $mail = $this->getMailerInstance();
    $mail->addAddress($email);
    $mail->isHTML(true);

    $mail->Subject = 'Open Journal System: Peer Review';
    $mail->Body = '<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
        <div style="margin:50px auto;width:70%;padding:20px 0">
          <div style="">
            <a href="'. $articleUrl .'" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Review this article</a>
          </div>
        </div>
      </div>';
    if ($mail->send()) {
      return true;
    }
    return false;
  }

  public function sendChangePasswordVerification($email, $url)
  {
    $mail = $this->getMailerInstance();
    $mail->addAddress($email);
    $mail->isHTML(true);

    $mail->Subject = 'Open Journal System change password verification';
    $mail->Body = '<a href="' . $url . '">Click here to change your password</a> <br>
        <p style="font-size: 12px; color: red">Ignore this message if you did not request to change your password.</p>';
    if ($mail->send()) {
      return true;
    }
    return false;
  }

  public function sendEmailOTP($email, $otp)
  {
    $mail = $this->getMailerInstance();
    $mail->addAddress($email);
    $mail->isHTML(true);

    $mail->Subject = 'Open Journal System email verification';
    $mail->Body = '<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
        <div style="margin:50px auto;width:70%;padding:20px 0">
          <div style="border-bottom:1px solid #eee">
            <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Open Journal System</a>
          </div>
          <p style="font-size:1.1em">Hi,</p>
          <p>Use the following OTP to complete your Sign In procedures. OTP is valid for 5 minutes</p>
          <h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">' . $otp . '</h2>
          <p style="font-size:0.9em;">Regards,<br />Open Journal System</p>
          <hr style="border:none;border-top:1px solid #eee" />
          <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
            <p>Open Journal System</p>
          </div>
        </div>
      </div>';
    if ($mail->send()) {
      return true;
    }
    return false;
  }

  private function getMailerInstance()
  {
    require base_path('vendor/autoload.php');
    $mail = new PHPMailer(true);
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->SMTPAuth = true;
    $mail->Username = env('MAIL_USERNAME');
    $mail->Password = env('MAIL_PASSWORD');
    $mail->SMTPSecure = 'tls';
    $mail->Port = env('MAIL_PORT');
    $mail->Host = env('MAIL_HOST');
    $mail->smtpConnect(array(
      'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
      )
    ));
    $mail->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    return $mail;
  }
}
