<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleComment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArticleCommentController extends Controller
{
    private $articleComment;

    public function __construct(ArticleComment $articleComment)
    {
        $this->articleComment = $articleComment;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $articleComments = $this->articleComment
                                ->all();

        return response()->json($articleComments);
    }

    public function getByArticleId($articleId) {
        $articleComments = $this->articleComment
                                ->where('article_id', $articleId)
                                ->get();

        return response()->json($articleComments);
    }

    public function getPublicArticleCommentsByUid($uid) {
        $article = Article::where('peer_review_id', $uid)
                            ->firstOrFail();

        $articleComments = $this->articleComment
                                ->select('email', 'comment', 'archived_at', 'created_at')
                                ->where('article_id', $article->id)
                                ->get();

        return response()->json($articleComments);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $articleComment = $this->articleComment
                                ->create($request->all());

        return response()->json($articleComment);
    }

    public function storePublicCommentByArticleUId(Request $request) {
        if (empty($request->aid) || empty($request->e) || empty($request->c)) {
            return response()->json(false);
        }
        
        $article = Article::where('peer_review_id', $request->aid)
                            ->firstOrFail();

        $articleId = $article->id;

        $email = $this->dehasher($request->e);

        $articleComment = $this->articleComment
                                ->create([
                                    "article_id" => $articleId,
                                    "email" => $email,
                                    "comment" => $request->c
                                ]);

        return response()->json($articleComment);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $articleComment = $this->articleComment
                            ->findOrFail($id);

        return response()->json($articleComment);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($id, Request $request)
    {
        $articleComment = $this->articleComment
                                ->findOrFail($id)
                                ->update($request->all());

        return response()->json($articleComment);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $articleComment = $this->articleComment
                                ->findOrFail($id);

        $articleComment->archived_at = Carbon::now();

        return response()->json($articleComment->save());
    }

    #region private
    private function dehasher($text) {
        return openssl_decrypt($text, 'AES-128-CTR', 'changepass', 0, '1234567891011121');
    }
    #endregion
}
