<?php

namespace App\Http\Controllers;

use App\Models\PageSection;
use App\Services\FileService;
use Illuminate\Http\Request;

class PageSectionController extends Controller
{
    private $pageSection;

    public function __construct(PageSection $pageSection)
    {
        $this->pageSection = $pageSection;
    }

    public function all()
    {
        $pageSections = $this->pageSection
                            ->orderBy('index', 'asc')
                            ->get();

        return response()->json($pageSections);
    }

    public function get($id)
    {
        $pageSection = $this->pageSection->findOrFail($id);
        return response()->json($pageSection);
    }

    public function getFirstByCategory($category) {
        $pageSection = $this->pageSection
                            ->where('category', $category)
                            ->firstOrFail();

        return response()->json($pageSection);
    }

    public function createOrUpdate(Request $req, $id = 0)
    {
        $fileService = new FileService();
        $filename = $fileService->saveFile($req, 'file');
        
        if ($id > 0) {
            $pageSection = $this->pageSection->findOrFail($id);

            $pageSection->category = $req->category;
            $pageSection->header = $req->header;
            $pageSection->subheader = $req->subheader;
            $pageSection->content = $req->content;
            
            // if filename is not empty, means new image added
            if (!empty($filename)) {
                // remove store image
                $fileService->removeFile($pageSection->picture);
                $pageSection->picture = $filename;
            }

            $pageSection->index = $req->index;
            $pageSection->save();
        }
        else {
            $inputs = $req->all();
            $inputs['picture'] = $filename;

            $pageSection = $this->pageSection->create($inputs);
        }

        return response()->json($pageSection);
    }

    public function delete($id)
    {
        $pageSection = $this->pageSection->findOrFail($id);

        if (!empty($pageSection->picture)) {
            $fileService = new FileService();
            $fileService->removeFile($pageSection->picture);
        }

        return response()->json($pageSection->delete());
    }
}
