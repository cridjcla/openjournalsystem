<?php

namespace App\Http\Controllers;

use App\DTOs\AuthorDto;
use App\Models\ArticleAuthor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $user;
    private $articleAuthor;

    public function __construct(User $user, ArticleAuthor $articleAuthor)
    {
        $this->user = $user;
        $this->articleAuthor = $articleAuthor;
    }

    public function all(Request $request)
    {
        $users = [];

        // check if "r" in query params exists
        // if so, filter by query param
        if (isset($request->r)) {
            $roleLabel = $request->r;

            switch ($roleLabel) {
                case 'reviewers':
                    $role = 4;
                    break;
                
                case 'authors':
                    $role = 3;
                    break;
                
                default:
                    $role = 1;
                    break;
            }

            if (!empty($request->sr)) {
                $users = $this->user
                                ->allActiveUser()
                                ->where('role_code', $role)
                                ->where('secondary_role', $request->sr)
                                ->orderBy('id', 'desc')
                                ->get();
            }
            else {
                $users = $this->user
                            ->allActiveUser()
                            ->where('role_code', $role)
                            ->orderBy('id', 'desc')
                            ->get();
            }
        }
        else {
            $users = $this->user
                        ->allActiveUser()
                        ->orderBy('id', 'desc')
                        ->get();
        }

        return response()->json($users);
    }

    public function getAuthors() {
        // merge user authors and article authors
        $userAuthors = $this->user
                        ->where('role_code', 3)
                        ->get();

        $authorsDto = $userAuthors->map(function(User $user) {
            return AuthorDto::fromUser($user);
        });
        
        $articleAuthors = $this->articleAuthor
                                ->get();

        $authorsDtoByArticleAuthors = $articleAuthors->map(function(ArticleAuthor $articleAuthor) {
            return AuthorDto::fromArticleAuthor($articleAuthor);
        });

        $authors = array();

        foreach ($authorsDto as $author) {
            array_push($authors, $author);
        }
        
        foreach ($authorsDtoByArticleAuthors as $author) {
            array_push($authors, $author);
        }

        return $authors;
    }

    public function get($id)
    {
        $user = $this->user->findOrFail($id);
        return response()->json($user);
    }

    public function create(Request $req)
    {
        // validate form inputs
        $req->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|unique:users',
            'password' => 'required|string|confirmed'
        ]);

        $inputs = $req->all();
        $inputs['password'] = bcrypt($req->password);
        $user = User::create($inputs);

        return response()->json($user);
    }

    public function createOrUpdate(Request $req, $id = 0)
    {
        $validationRules = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
        ];

        // update users
        if ($id > 0) {
            // if email request exists and diff to current email, add email validation
            if (!empty($req->email) && !empty($req->currentEmail) && $req->email != $req->currentEmail) {
                $validationRules['email'] = 'required|string|unique:users';
            }

            // validate form
            $req->validate($validationRules);

            $user = $this->user
                        ->findOrFail($id)
                        ->update($req->all());
        }

        return response()->json($user);
    }

    public function updateRoleById($id, Request $req)
    {
        $auth = Auth::user();

        if ($auth->role_code !== 1 && $auth->role_code !== 2) {
            return response()->json(false, 401);
        }

        $user = $this->user->findOrFail($id);
        $user->role_code = $req->role_code;
        return response()->json($user->save());
    }

    public function delete($id)
    {
        $user = $this->user->findOrFail($id);
        $user->archived = true;
        return response()->json($user->save());
    }
}