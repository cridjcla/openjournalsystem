<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    private $contact;

    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    public function all() {
        $contacts = $this->contact
                        ->orderBy('created_at', 'desc')
                        ->get();
                        
        return response()->json($contacts);
    }

    public function get($id) {
        $contact = $this->contact->findOrFail($id);
        return response()->json($contact);
    }

    public function create(Request $request) {
        $contact = $this->contact->create($request->all());
        return response()->json($contact);
    }

    public function update($id, Request $request) {
        $contact = $this->contact
                        ->where('id', $id)
                        ->update($request->all());

        return response()->json($contact);
    }

    public function remove($id) {
        $contact = $this->contact->findOrFail($id);
        return response()->json($contact->delete());
    }
}
