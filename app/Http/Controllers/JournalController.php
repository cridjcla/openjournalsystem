<?php

namespace App\Http\Controllers;

use App\Models\Journal;
use App\Services\FileService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JournalController extends Controller
{
    private $journal;
    private $authUserId;

    public function __construct(Journal $journal) {
        $this->journal = $journal;
        $this->authUserId = Auth::id();
    }

    public function all() {
        $journals = $this->journal
                        ->scopeNotArchived()
                        ->orderBy('created_at', 'desc')
                        ->get();

        return response()->json($journals);
    }

    public function getPublicJournals() {
        $journals = $this->journal
                        ->scopePublished()
                        ->orderBy('published_at', 'desc')
                        ->get();

        return response()->json($journals);
    }

    public function get($id) {
        $journal = $this->journal
                        ->findOrFail($id);
                        
        return response()->json($journal);
    }

    public function create(Request $request) {
        // TODO: implement the log, response model, and catch errors
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'nullable|string'
        ]);

        $fileService = new FileService();
        $filename = $fileService->saveFile($request, 'file');

        $inputs = $request->all();
        
        if (!empty($filename)) {
            $inputs['cover_image'] = $filename;
        }

        $journal = $this->journal
                        ->create($inputs);

        return response()->json($journal);
    }
    
    public function update($id, Request $request) {
        $request->validate([
            'title' => 'required|string'
        ]);

        $inputs = $request->all();

        // TODO: implement the log, response model, and catch errors
        $journal = $this->journal
                        ->findOrFail($id)
                        ->update($inputs);

        return response()->json($journal);
    }
    
    public function updatePublishById($id, Request $request) {
        $inputs = $request->all();
        
        if (!empty($inputs['status']) && $inputs['status'] == 'publish') {
            $inputs['published_at'] = Carbon::now();
            $inputs['published_by'] = $this->authUserId;
        }
        else {
            $inputs['published_at'] = null;
            $inputs['published_by'] = null;
        }

        // TODO: implement the log, response model, and catch errors
        $journal = $this->journal
                        ->findOrFail($id)
                        ->update($inputs);

        return response()->json($journal);
    }

    public function delete($id) {
        $journal = $this->journal->findOrFail($id);
        $journal->archived_at = Carbon::now();
        $journal->archived_by = Auth::id();
        return response()->json($journal->save());
    }
}
