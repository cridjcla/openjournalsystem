<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Article;
use App\Models\Journal;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ArticleAuthor;
use App\Models\User;
use App\Services\FileService;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    private $article;
    private $articleAuthors;
    private $user;

    public function __construct(Article $article, ArticleAuthor $articleAuthors, User $user) {
        $this->article = $article;
        $this->articleAuthors = $articleAuthors;
        $this->user = $user;
    }

    public function all() {
        $authUser = Auth::user();

        // if role is author, show only his articles
        if ($authUser->role_code === 3) {
            $articles = $this->article
                        ->allActive()
                        ->with('articleAuthors')
                        ->where('user_id', $authUser->id)
                        ->get();
        }
        else {
            $articles = $this->article
                        ->allActive()
                        ->with('articleAuthors')
                        ->get();
        }

        return response()->json($articles);
    }

    public function get($id) {
        $article = $this->article
                        ->with('articleAuthors')
                        ->findOrFail($id);
                        
        return response()->json($article);
    }
    
    public function getPublicArticlesByJournalId(Request $request) {
        $inputs = $request->all();

        if (empty($inputs['j'])) {
            return response()->json([]);
        }

        $journalId = $inputs['j'];


        if (!empty($journalId)) {
            $articles = $this->article
                            ->allActive()
                            ->with('articleAuthors')
                            ->where('journal_id', $journalId)
                            ->where('published_at', '!=', null)
                            ->get();
        }


        return response()->json($articles);
    }

    public function getByUid($uid) {
        // show article if status is 3 and valid preview Id
        $article = $this->article
                        ->where('peer_review_id', $uid)
                        ->where('status', 3)
                        ->firstOrFail();

        return response()->json($article);
    }

    public function create(Request $request) {
        // TODO: implement the log, response model, and catch errors
        $inputs = $request->all();
        $authors = $inputs['authors'];
        $userId = Auth::id();
        $inputs['user_id'] = $userId;
        
        // store article first
        $article = $this->article->create($inputs);

        // store authors
        if (!empty($article['id']) && !empty($authors)) {
            foreach ($authors as $author) {
                // TODO: catch if there is an error in author later
                $author['user_id'] = $userId;
                $author['article_id'] = $article['id'];
                $newAuthor = $this->articleAuthors->create($author);
            }
        }

        return response()->json($article);
    }

    public function update($id, Request $request) {
        $auth = Auth::user();

        $validatedInputs = $request->validate([
            'title' => 'required',
            'introduction' => 'required',
            'abstract' => 'required',
            'method' => 'required',
            'result' => 'required',
            'conclusion' => 'required',
            'references' => 'required',
            'recommendation' => 'required',
            'acknowledgement' => 'required',
            'keywords' => 'string',
            'comment_summary' => 'string',
            'total_resubmit' => 'integer',
        ]);

        $article = $this->article->findOrFail($id);

        if (empty($article) || $article->user_id != $auth->id) {
            return response()->json(false);
        }

        $result = $article->update($validatedInputs);
        return response()->json($result);
    }

    public function updateColumn($id, Request $request) {
        $validatedInputs = $request->validate([
            'comment_summary' => 'string',
            'total_resubmit' => 'integer',
            'journal_id' => 'integer',
        ]);

        $article = $this->article->findOrFail($id);
        $result = $article->update($validatedInputs);

        if ($result && isset($request->comment_summary)) {
            // send notification
            try {
                $this->sendResubmitNotificationToAuthor($article);
            } catch (\Throwable $th) {
                //throw $th;
            }
        }

        return response()->json($result);
    }

    public function updateStatusById(Request $request) {
        // add journalal id validation
        if (empty($request->id) || $request->id == 0) {
            return response()->json(array(
                'status' => false,
                'error'  => 'Journal cannot found'
            ));
        }

        $authUser = Auth::user();
        $approver = $authUser->first_name . ' ' . $authUser->last_name;
        $article = $this->article->findOrFail($request->id);
        $article->status = $request->status;
        $article->published_at = $request->published_at;

        // send notification by status id 
        try {
            $this->sendNotificationByStatusId($article, $approver);
        } catch (\Throwable $th) {
            // add logger instead
            // throw $th;
        }

        return response()->json($article->save());
    }

    public function updatePublicStatusByUid(Request $request) {
        // add journalal id validation
        if (empty($request->id) || $request->id == 0) {
            return response()->json(array(
                'status' => false,
                'error'  => 'Journal cannot found'
            ));
        }

        $article = $this->article->findOrFail($request->id);
        $article->status = $request->status;

        return response()->json($article->save());
    }

    public function updateManagingEditorById(Request $request) {
        $validatedData = $request->validate([
            'id' => 'required',
            'managing_editor' => 'required',
        ]);

        $article = $this->article
                        ->findOrFail($request->id)
                        ->update($validatedData);

        return response()->json($article);
    }

    public function updateFileById(Request $request) {
        $auth = Auth::user();
        if ($auth->role_code != 4 || $auth->secondary_role != 'layout_artist' || empty($request->id)) {
            return response()->json(false);
        }

        $request->validate([
            'file' => 'required|mimes:pdf'
        ]);

        $article = $this->article->findOrFail($request->id);
        $fileService = new FileService();
        $filename = $fileService->saveFile($request, 'file');

        if (empty($filename)) {
            return response()->json(false);
        }

        if (!empty($article->file)) {
            $fileService->removeFile($article->file);
        }

        $article->file = $filename;
        return response()->json($article->save());
    }

    public function sendEmailToPeerReview(Request $request) {
        $validatedData = $request->validate([
            'id' => 'required',
            'email' => 'required',
        ]);
        
        $article = $this->article
                        ->findOrFail($validatedData['id']);

        if (empty($article->journalId)) {
            $article->peer_review_id = Str::uuid();
            $article->save();
        }
        
        $url = url('/') . '#/article/peer?aid=' . $article->peer_review_id . '&e=' . $this->hasher($validatedData['email']);

        $mailController = new MailController;

        if ($mailController->sendArticleReviewForApproval($url, $validatedData['email'])) {
            return response()->json('Email Verification Sent');
        }

        return response()->json('Email send error');
    }

    public function delete($id) {
        $article = $this->article->findOrFail($id);
        $article->archived_at = Carbon::now();
        $article->archived_by = Auth::user()->id;
        return response()->json($article->save());
    }

    #region private
    private function sendNotificationByStatusId($article, $approver) {
        if (empty($article->status)) {
            return false;
        }

        // send email for author and next reviewer
        $author = $this->user->findOrFail($article->user_id);
        $authorEmail = $author->email;

        $approversEmailMessage = "New article approved! Please check out article {$article->title}.";

        // all next approvers contains specific role code will receive the email
        // status 1 = eic, 2 = iptbm, 3 = (eic, select managing editor) (depends on eic selected managing editor), 
        // 4 = managing editor, 5 = eic (for publishing) 
        switch ($article->status) {
            case 1:
                $approvers = $this->getApproversBySecondaryRole('eic');
                $authorEmailMessage = "Congratulations! Your article, {$article->title}, has been approved by Admin [{$approver}]. It's moving on to the next stage of the review process.";
                break;

            case 2:
                $approvers = $this->getApproversBySecondaryRole('iptbm');
                $authorEmailMessage = "Congratulations! Your article, {$article->title}, has been approved by Editor-in-Chief [{$approver}]. It's moving on to the next stage of the review process.";
                break;
            
            case 3:
                $approvers = $this->getApproversBySecondaryRole('eic');
                $authorEmailMessage = "Congratulations! Your article, {$article->title}, has been approved by IPTBM [{$approver}]. It's moving on to the next stage of the review process.";
                break;

            case 4:
                $approvers = $this->getApproversBySecondaryRole('managing_editor');
                $authorEmailMessage = "Congratulations! Your article, {$article->title}, has been approved by Peer Reviewer [{$approver}]. It's moving on to the next stage of the review process.";
                break;

            case 5:
                $approvers = $this->getApproversBySecondaryRole('eic');
                $authorEmailMessage = "Congratulations! Your article, {$article->title}, has been approved by Managing Editor [{$approver}]. It's moving on to the next stage of the review process.";
                break;

            case 6:
                $approvers = [];
                $authorEmailMessage = "Congratulations! Your article, {$article->title}, has been published by Editor-in-Chief [{$approver}].";
                $approversEmailMessage = '';
                break;

            case 7:
                $approvers = [];
                $authorEmailMessage = "Your article, {$article->title}, has been rejected by [{$approver}].";
                $approversEmailMessage = '';
                break;
                
            default:
                $approvers = [];
                $authorEmailMessage = '';
                $approversEmailMessage = '';
                break;
        }

        if (!empty($authorEmailMessage)) {
            $authorEmailMessage .= '<br><br><a href="https://jsit.com.ph" style="text-decoration: none; padding: 10px 16px; background-color: #279734; color: #fff; display: inline-block; font-size: 14px; border-radius: 4px;">View Article</a>';
        }
        
        if (!empty($approversEmailMessage)) {
            $approversEmailMessage .= '<br><br><a href="https://jsit.com.ph" style="text-decoration: none; padding: 10px 16px; background-color: #279734; color: #fff; display: inline-block; font-size: 14px; border-radius: 4px;">View Article</a>';
        }

        // validate and send the emails
        $mailController = new MailController;

        if (!empty($authorEmail)) {
            $sendEmailResponse = $mailController->sendEmailUsingEmailMessage($authorEmail, $authorEmailMessage, 'Article Updates');
        }

        if (!empty($approvers)) {
            foreach ($approvers as $approver) {
                $sendEmailResponse = $mailController->sendEmailUsingEmailMessage($approver->email, $approversEmailMessage, 'Article Updates');
            }
        }
    }

    private function sendResubmitNotificationToAuthor($article) {
        $author = $this->user->findOrFail($article->user_id);
        $authorEmail = $author->email;
        
        $authorEmailMessage = "Your article, {$article->title}, is requesting for revision. Please check the comments and make the necessary changes.";

        // add action btn
        $authorEmailMessage .= '<br><br><a href="https://jsit.com.ph" style="text-decoration: none; padding: 10px 16px; background-color: #279734; color: #fff; display: inline-block; font-size: 14px; border-radius: 4px;">View Article</a>';

        $mailController = new MailController;

        if (!empty($authorEmail)) {
            $sendEmailResponse = $mailController->sendEmailUsingEmailMessage($authorEmail, $authorEmailMessage, 'Article Updates');
        }
    }

    private function getApproversBySecondaryRole($secondaryRole) {
        $approvers = $this->user
                        ->where('role_code', 4)
                        ->where('secondary_role', $secondaryRole)
                        ->get();

        return $approvers;
    }

    private function hasher($text) {
        return openssl_encrypt($text, 'AES-128-CTR', 'changepass', 0, '1234567891011121');
    }

    private function dehasher($text) {
        return openssl_decrypt($text, 'AES-128-CTR', 'changepass', 0, '1234567891011121');
    }
    #endregion
}
