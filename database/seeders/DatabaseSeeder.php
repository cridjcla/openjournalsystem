<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'first_name' => 'JSIT',
                'middle_name' => ' ',
                'last_name' => 'ADMIN',
                'email' => 'admin@admin.com',
                'password' => bcrypt('password'),
                'contact_number' => '09225178392',
                'role_code' => '1',
                'email_verified_at' => new \DateTime(),
                'created_at' => new \DateTime(),
            ],
            [
                'first_name' => 'Czyril Roi',
                'middle_name' => 'David',
                'last_name' => 'Jacla',
                'email' => 'user@user.com',
                'password' => bcrypt('password'),
                'contact_number' => '09225178392',
                'role_code' => '3',
                'email_verified_at' => new \DateTime(),
                'created_at' => new \DateTime(),
            ],
        ]);
// Editors and Reviewers
        DB::table('users')->insert([
            [
                'first_name' => 'Jasmin',
                'middle_name' => 'Sagum',
                'last_name' => 'Villanueva',
                'email' => 'editorjsit2022@psau.edu.ph',
                'password' => bcrypt('researchjournal2022'),
                'contact_number' => '09362936204',
                'role_code' => '4',
                'secondary_role' => 'eic',
                'position' => 'Editor-in-Chief',
                'affiliate' => 'Pampanga State Agricultural University',
                'email_verified_at' => new \DateTime(),
                'created_at' => new \DateTime(),
            ],
            [
                'first_name' => 'Jerah Mystica',
                'middle_name' => 'Bolos',
                'last_name' => 'Novenario',
                'email' => 'jsit_managingeditor@psau.edu.ph',
                'password' => bcrypt('@psau2025'),
                'contact_number' => '09151715890',
                'role_code' => '4',
                'secondary_role' => 'managing_editor',
                'position' => 'Managing Editor',
                'affiliate' => 'Pampanga State Agricultural University',
                'email_verified_at' => new \DateTime(),
                'created_at' => new \DateTime(),
            ],
            [
                'first_name' => 'Robby',
                'middle_name' => 'De Jesus',
                'last_name' => 'Lalu',
                'email' => 'jsit_copyeditor@psau.edu.ph',
                'password' => bcrypt('@psau2025'),
                'contact_number' => '09272350503',
                'role_code' => '4',
                'secondary_role' => 'iptbm',
                'position' => 'IPTBM Reviewer',
                'affiliate' => 'Pampanga State Agricultural University',
                'email_verified_at' => new \DateTime(),
                'created_at' => new \DateTime(),
            ],
            [
                'first_name' => 'Reynaldo',
                'middle_name' => 'Fernandez',
                'last_name' => 'Punu III',
                'email' => 'jsit_layoutartist@psau.edu.ph',
                'password' => bcrypt('@psau2025'),
                'contact_number' => '09454098776',
                'role_code' => '4',
                'secondary_role' => 'layout_artist',
                'position' => 'Layout Artist',
                'affiliate' => 'Pampanga State Agricultural University',
                'email_verified_at' => new \DateTime(),
                'created_at' => new \DateTime(),
            ],
        ]);
        
        DB::table('page_sections')->insert([
            [

                'category' => 'about',
                'header' => 'ABOUT THE WEBSITE',
                'subheader' => '',
                'content' => '<ul>
            <li>
                The Journal of Science, Innovation, and Technology is a prestigious peer-reviewed publication affiliated
                with Pampanga State Agricultural University. Its primary mission is to provide a platform for disseminating
                high-quality research findings by accomplished and diligent faculty researchers across science, technology,
                and education domains.
            </li>
            <br/>
                <li>
                    Within the pages of this journal, readers can expect to encounter a rich tapestry of articles that span an
                    extensive array of academic disciplines. The journal offers a comprehensive and inclusive perspective on
                    contemporary research, encompassing not only the natural and physical sciences but also delving into the
                    realms of the social sciences, humanities, and beyond.
                </li>
            <br/>
                <li>
                    Each manuscript featured in this journal is a product of meticulous and exhaustive investigation undertaken
                    to uncover new insights and advance knowledge in their respective fields. The contributions made through
                    these submissions play a pivotal role in enhancing the body of literature and fostering innovation across
                    diverse research areas.
                </li>
            <br/>
                <li>
                    The Journal of Science, Innovation, and Technology stands as a beacon of intellectual rigor and scholarly
                    excellence, dedicated to fostering interdisciplinary collaboration and promoting the advancement of
                    knowledge across the spectrum of science, technology, and education. Its pages testify to faculty
                    researchers\' tireless efforts to push the boundaries of human understanding and propel society forward
                    through their groundbreaking discoveries.
                </li>
            </ul>'
            ],
            [
                'category' => 'open_access_policy',
                'header' => 'OPEN ACCESS POLICY',
                'subheader' => '',
                'content' => 'The Journal of Science, Innovation, and Technology, the official research journal of Pampanga State Agricultural University, adopts a Full Open Access policy to ensure maximum visibility and accessibility of research findings. Under this policy, all articles published in the journal will be made immediately available online, free of charge, with no embargo period. Authors retain copyright of their work and grant the journal a license to publish, allowing others to freely access, share, and build upon the work while providing proper attribution.'
            ],
            [
                'category' => 'copyright_policy',
                'header' => 'COPYRIGHT',
                'subheader' => '',
                'content' => '<li>
                            The Author hereby transfers the copyright of the article to the Journal of Science, Innovation, and
                            Technology (JSIT), hereinafter called the Transferee. This transfer grants the Transferee the exclusive
                            right to reproduce and distribute the Work in any form, including but not limited to reprints, translations,
                            photographic reproductions, electronic formats, and any other reproductions of similar nature.
                            </li>
                            <br />
                            <li>
                                The Author agrees not to publish the Work elsewhere without prior written permission from the Transferee.
                                The Author warrants that the Work is original, except for excerpts from copyrighted works included with
                                appropriate permissions, contains no libelous statements, and does not infringe upon any copyright,
                                trademark, patent, statutory right, or proprietary right of others.
                            </li>
                            <br />
                            <li>
                                The Author accepts responsibility for releasing this material on behalf of all co-authors. In return, the
                                Author retains the following rights: (1) all intellectual property rights other than copyright; (2) the
                                right to use the Work, in whole or in part, in future works with proper acknowledgment to the Transferee;
                                (3) the right to reproduce and distribute copies for academic, non-commercial purposes; (4) the
                                responsibility to ensure ethical standards regarding plagiarism, with the understanding that violations may
                                result in rejection and a one-year publishing ban; and (5) the assurance that the Work will not be submitted
                                elsewhere prior to the Transferee\'s decision. The Author agrees to indemnify the JSIT against any breach of
                                these warranties and represents having the authority to execute this agreement.
                            </li>'
            ],
            [
                'category' => 'publication_ethics',
                'header' => 'Publication Ethics',
                'subheader' => '',
                'content' => '<span class="text-uppercase text-bold">
                                    Authors
                                </span>
                            <ul>
                                    <li>
                                        <span class="text-bold">
                                            Originality and Plagiarism:
                                        </span>
                                        All manuscripts submitted must be original works. Authors are responsible for ensuring that their work is
                                        free from plagiarism, including self-plagiarism. Proper attribution and citation of all sources are
                                        required. Likewise, the submitted papers will undergo plagiarism check to ensure that the papers are free
                                        from any illegal misuse.
                                    </li>
                                    <br />
                                    <li>
                                        <span class="text-bold">
                                            Data Integrity:
                                        </span>
                                        Authors must ensure that their data is accurate, reliable, and presented honestly. Fabrication,
                                        falsification, or manipulation of data is strictly prohibited.
                                    </li>
                                    <br />
                                    <li>
                                        <span class="text-bold">
                                            Ethical Compliance:
                                        </span>
                                        For research involving human participants or animals, authors must provide evidence of ethical approval from
                                        relevant institutional review boards or ethics committees.
                                    </li>
                                    <br />
                                </ul>
                                <span class="text-uppercase text-bold">
                                    Reviewers
                                </span>
                                <ul>
                                    <li>
                                        <span class="text-bold">
                                            Confidentiality:
                                        </span>
                                        Reviewers must treat all manuscripts as confidential documents and should not disclose any details of a
                                        manuscript or discuss its content with others.
                                    </li>
                                    <br />
                                    <li>
                                        <span class="text-bold">
                                            Objectivity and Constructive Feedback:
                                        </span>
                                        Reviews should be conducted objectively, and feedback should be constructive, specific, and respectful to
                                        help authors improve their work.
                                    </li>
                                    <br />
                                    <li>
                                        <span class="text-bold">
                                            Timeliness:
                                        </span>
                                        Reviewers are expected to complete their reviews within the set timeframe of the JSIT to ensure timely
                                        publication.
                                    </li>
                                    <br />
                                </ul>
                                <span class="text-uppercase text-bold">
                                    Editors
                                </span>
                                <ul>
                                    <li>
                                        <span class="text-bold">
                                            Fairness and Impartiality:
                                        </span>
                                        Editors must evaluate manuscripts solely based on their academic merit, without regard to race, gender,
                                        sexual orientation, religious belief, ethnic origin, or political philosophy of the authors.
                                    </li>
                                    <br />
                                    <li>
                                        <span class="text-bold">
                                            Confidentiality:
                                        </span>
                                        Editors must maintain the confidentiality of submitted manuscripts and refrain from disclosing information
                                        about submissions to anyone other than the corresponding author, reviewers, and publisher.
                                    </li>
                                    <br />
                                    <li>
                                        <span class="text-bold">
                                            Publication Desicions:
                                        </span>
                                        Decisions to accept or reject a manuscript should be based on its importance, originality, clarity, and
                                        relevance to the journal\'s scope, as well as the validity of its findings.
                                    </li>
                                    <br />
                                    <li>
                                        <span class="text-bold">
                                            Addressing Ethical Concerns:
                                        </span>
                                        Editors are responsible for taking responsive measures if ethical concerns arise regarding a submitted or
                                        published manuscript. This includes issuing corrections, retractions, or expressions of concern when
                                        necessary.
                                    </li>
                                    <br />
                                </ul>
                                <span class="text-uppercase text-bold">
                                    Publisher
                                </span>
                                <ul>
                                    <li>
                                        <span class="text-bold">
                                            Maintaining Publication Ethics:
                                        </span>
                                        The publisher works closely with editors to uphold the integrity of the journal\'s editorial processes and
                                            ensure compliance with ethical guidelines.
                                        </li>
                                        <br />
                                        <li>
                                            <span class="text-bold">
                                                Correction of Errors:
                                            </span>
                                            The publisher is responsible for addressing errors, omissions, or inaccuracies in published articles in a
                                            timely manner and issuing corrections or retractions when required.
                                        </li>
                                        <br />
                                        <li>
                                            <span class="text-bold">
                                                Archiving and Accessibility:
                                            </span>
                                            The publisher ensures that all published content is securely archived and accessible to readers through
                                            convenient and secured repositories.
                                        </li>
                                        <br />
                                    </ul>
                                    <span class="text-uppercase text-bold">
                                        Readers
                                    </span>
                                    <ul>
                                        <li>
                                            <span class="text-bold">
                                                Respect for Intellectual Property:
                                            </span>
                                            Readers must respect the intellectual property rights of authors by properly citing or referencing works
                                            they use in their own research or publications.
                                        </li>
                                        <br />
                                        <li>
                                            <span class="text-bold">
                                                Appropriate Use of Content:
                                            </span>
                                            Readers must use the journal\'s content for ethical purposes such as academic research, education, or
                                            professional development and avoid using it for unethical purposes, such as plagiarism or misrepresentation.
                                        </li>
                                        <br />
                                        <li>
                                            <span class="text-bold">
                                                Reporting Ethical Concerns:
                                            </span>
                                            Readers who suspect misconduct in any published work are encouraged to report their concerns to the
                                            journal\'s editorial team with sufficient evidence to facilitate investigation.
                                        </li>
                                        <br />
                                    </ul>'
            ],
            [
                'category' => 'plagiarism_screening',
                'header' => 'PLAGIARISM SCREENING',
                'subheader' => '',
                'content' => '<span>
                                To ensure all published papers are free from any form of illegal use, a plagiarism check will be conducted as
                                part of the screening process.
                            </span>
                            <ul>
                                <li>
                                    The JSIT will submit the paper to the university\'s Knowledge Management Unit (KMU) for plagiarism
                                    scanning.
                                </li>
                                <br />
                                <li>
                                    The paper will be evaluated using Turnitin software, which identifies similarities with other online
                                    sources.
                                </li>
                                <br />
                                <li>
                                    If the similarity index exceeds 11%, the paper will require further editing based on KMU guidelines. In
                                    such cases, it will be returned to the author(s) for revision.
                                </li>
                                <br />
                                <li>
                                    If the similarity index is 10% or lower, the paper will be accepted, and the results will be emailed to
                                    the JSIT team for confirmation.
                                </li>
                                <br />
                                <li>
                                    Once approved, the paper will move to the next stage of the process.
                                </li>
                                <br />
                            </ul>'
            ],
            [
                'category' => 'grammar_screening',
                'header' => 'GRAMMAR SCREENING',
                'subheader' => '',
                'content' => '<span>
                                In addition to plagiarism checks, all papers submitted to JSIT will undergo a grammar review to ensure they are
                                free of writing errors that could affect readability.
                            </span>
                            <ul>
                                <li>
                                    The papers will be sent to the Knowledge Management Unit (KMU) for grammar checking.
                                </li>
                                <br />
                                <li>
                                    Grammarly software will be used to identify areas for improvement in the text.
                                </li>
                                <br />
                                <li>
                                    If the paper receives a score of 89 or lower, it will be returned to the author(s) for revisions based on
                                    Grammarly\'s feedback.
                                </li>
                                <br />
                                <li>
                                    On the other hand, if the paper achieves a score of 90 or higher, it will be deemed acceptable and proceed
                                    to the next stage of the process.
                                </li>
                                <br />
                            </ul>'
            ],
        ]);
    }
}
