<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->id();
            $table->string('subject')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('issn')->nullable();
            $table->string('online_issn')->nullable();
            $table->string('cover_image')->nullable();
            $table->timestamp('archived_at')->nullable();
            $table->bigInteger('archived_by')->unsigned()->nullable()->index();
            $table->timestamp('published_at')->nullable();
            $table->bigInteger('published_by')->unsigned()->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journals');
    }
}
