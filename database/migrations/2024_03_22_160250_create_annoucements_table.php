<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnoucementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annoucements', function (Blueprint $table) {
            $table->id();
            $table->string('category')->nullable();
            $table->string('header')->nullable();
            $table->string('subheader')->nullable();
            $table->text('content')->nullable();
            $table->string('banner')->nullable();
            $table->text('pictures')->nullable();
            $table->smallInteger('index')->default(1);
            $table->boolean('archive')->default(false);
            $table->smallInteger('status')->default(0);
            $table->boolean('approved')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annoucements');
    }
}
