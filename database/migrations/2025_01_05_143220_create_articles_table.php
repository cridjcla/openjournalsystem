<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->references('id')->on('users');
            $table->bigInteger('journal_id')->references('id')->on('journals')->nullable();
            $table->bigInteger('managing_editor')->references('id')->on('users')->nullable();
            $table->string('title');
            $table->longText('introduction')->nullable();
            $table->text('abstract')->nullable();
            $table->longText('method')->nullable();
            $table->longText('result')->nullable();
            $table->longText('conclusion')->nullable();
            $table->longText('references')->nullable();
            $table->longText('recommendation')->nullable();
            $table->longText('acknowledgement')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->bigInteger('published_by')->unsigned()->nullable()->index();
            $table->timestamp('archived_at')->nullable();
            $table->bigInteger('archived_by')->unsigned()->nullable()->index();
            $table->tinyInteger('status')->default(0);
            $table->string('peer_review_id')->nullable();
            $table->text('keywords')->nullable();
            $table->text('comment_summary')->nullable();
            $table->tinyInteger('total_resubmit')->default(0);
            $table->string('file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
