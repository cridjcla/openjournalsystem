import { Dialog } from "quasar"

const confirm = (title = 'Confirm', message, callback, onCancelCallback, onDismissCallback) => {
    Dialog.create({
        title: title,
        message: message ?? 'Would you like to delete this row?',
        focus: 'cancel',
        cancel: true,
        persistent: true
    })
    .onOk(() => callback())
    .onCancel(() => !onCancelCallback ? false : onCancelCallback())
    .onDismiss(() => !onDismissCallback ? false : onDismissCallback());
}

const confirmDelete = (callback) => {
    return confirm('Confirm', 'Would you like to delete this row?', callback);
}

export default {
    confirm,
    confirmDelete
}