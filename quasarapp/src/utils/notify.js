import { Notify } from "quasar"

const setup = (status, successMessage, errorMessage) => {
    Notify.create({
        message: status ? successMessage ?? 'Successfully saved' : errorMessage ?? 'Something went wrong, please try again later',
        color: status ? 'positive' : 'negative',
        position: 'top'
    });
}

export default {
    setup
}