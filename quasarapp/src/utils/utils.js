import { date, exportFile } from "quasar";

export default {
    setErrors(err) {
        return err.response.data.errors;
    },

    getErrorMessage(error) {
        return error ? error[0] : false;
    },

    isError(error) {
        return error != undefined;
    },

    filterFn(val, origList) {
        if (val === "") return origList;

        try {
            return origList.filter(
                (v) => v.toLowerCase().indexOf(val.toLowerCase()) > -1
            );
        } catch (error) {
            return origList.filter(
                (v) => v.label.toLowerCase().indexOf(val.toLowerCase()) > -1
            );
        }
    },

    wrapCsvValue(val, formatFn, row) {
        let formatted = formatFn !== void 0 ? formatFn(val, row) : val;
        formatted = formatted === void 0 || formatted === null ? "" : String(formatted);
        formatted = formatted.split('"').join('""');
        return `"${formatted}"`;
    },

    formatDate(rawDate) {
        return date.formatDate(rawDate, 'MM/DD/YYYY');
    },

    formatDateTimeRange(rawDate) {
        let date = new Date(rawDate);
        date.setHours(0, 0, 0, 0);
        return date.getTime();
    },

    formatTime(rawTime) {
        let fakeDate = 'Wed, 09 Aug 2022 ' + rawTime;
        return date.formatDate(fakeDate, 'hh:mm A');
    },

    formatMoney(number, decPlaces, decSep, thouSep) {
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSep = typeof decSep === "undefined" ? "." : decSep;
        thouSep = typeof thouSep === "undefined" ? "," : thouSep;
        var sign = number < 0 ? "-" : "";
        var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
        var j = (j = i.length) > 3 ? j % 3 : 0;

        return sign +
            (j ? i.substr(0, j) + thouSep : "") +
            i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
            (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
    },

    formatMoneyValue(num, sign = 'PHP') {
        var p = num.toFixed(2).split(".");
        return sign + p[0].split("").reverse().reduce(function (acc, num, i, orig) {
            return num + (num != "-" && i && !(i % 3) ? "," : "") + acc;
        }, "") + "." + p[1];
    },

    exportTable(columns, list, filename = 'file') {
        const content = [
            columns.map((col) => this.wrapCsvValue(col.label)),
        ]
            .concat(
                list.map((row) =>
                    columns
                        .map((col) =>
                            this.wrapCsvValue(
                                typeof col.field === "function"
                                    ? col.field(row)
                                    : row[
                                    col.field === void 0
                                        ? col.name
                                        : col.field
                                    ],
                                col.format,
                                row
                            )
                        )
                        .join(",")
                )
            )
            .join("\r\n");

        const status = exportFile(filename, content, "text/csv");

        if (status !== true) {
            $q.notify({
                message: "Browser denied file download...",
                color: "negative",
                icon: "warning",
            });
        }
    },

    setDateRangeLabel(val) {
        if (!val) return '';
        if (typeof val == 'string') return val;
        return `${val.from} - ${val.to}`;
    },

    verifyScope(requiredScope = [], reverse = false) {
        let user = localStorageUser.getLocalStorageUser();
        if (!user) return true;

        // Superadmin
        if (!reverse) requiredScope.push(1);
        let roleId = parseInt(user.role);
        return reverse ? !requiredScope.includes(roleId) : requiredScope.includes(roleId);
    },

    isInDateRange(selectedDate, date) {
        if (selectedDate) {
            if (typeof selectedDate == 'string')
                return new Date(selectedDate).toLocaleDateString() == new Date(date).toLocaleDateString();

            let from = this.formatDateTimeRange(selectedDate.from);
            let to = this.formatDateTimeRange(selectedDate.to);
            let selected = this.formatDateTimeRange(date);

            return from <= selected && selected <= to;
        }

        return false;
    },

    timeAgo(date) {
        const seconds = Math.floor((new Date() - date) / 1000);

        let interval = Math.floor(seconds / 31536000);
        if (interval > 1) {
            return interval + ' years ago';
        }

        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + ' months ago';
        }

        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + ' days ago';
        }

        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + ' hours ago';
        }

        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + ' minutes ago';
        }

        if (seconds < 10) return 'just now';

        return Math.floor(seconds) + ' seconds ago';
    },

    setPictureUrl(pictures) {
        if (pictures == null || pictures == '' || pictures == undefined) {
            return ''
        }

        // let multiple = pictures.split(',').length > 0
        
        // if (multiple) {
        //     // intended to be empty
        // }

        if (process.env.DEV) {
            return `http://127.0.0.1:8000/files/${pictures}`
        }

        return `/files/${pictures}`
    },

    getObjectKeyByValue(obj, val) {
        return Object.keys(obj).find(key => obj[key] == val)
    }
}