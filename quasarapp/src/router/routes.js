const routes = [
  {
    path: '/',
    component: () => import('layouts/LandingLayout.vue'),
    children: [
      //Public Route
      { path: '/', component: () => import('src/pages/LandingPage.vue') },

      
      { path: '/about', component: () => import('src/pages/AboutPage.vue') },
      // { path: '/scope', component: () => import('src/pages/ScopePage.vue')},
      { path: '/editorial-board', component: () => import('src/pages/EditorialPage.vue')},

      { path: '/open-access-policy', component: () => import('src/pages/OpenAccessPolicyPage.vue') },
      { path: '/copyright-policy', component: () => import('src/pages/CopyrightPage.vue') },
      { path: '/publication-ethics', component: () => import('src/pages/PublicationEthicsPage.vue') },
      { path: '/plagiarism-screening', component: () => import('src/pages/PlagiarismScreeningPage.vue') },
      { path: '/grammar-screening', component: () => import('src/pages/GrammarScreeningPage.vue') },      

      { path: '/contact', component: () => import('src/pages/ContactPage.vue') },

      { path: '/journals', component: () => import('src/pages/JournalPage.vue')},
      { path: '/journals/:journalId/articles', component: () => import('src/pages/ArticleListPage.vue')},
      { path: '/article/peer', component: () => import('src/pages/ArticlePeerReviewPage.vue')},
      { path: '/:articleId/:articleTitle', component: () => import('src/pages/ArticlePage.vue')},
      
      { path: '/change-password', component: () => import('pages/authentication/ChangePasswordPage.vue') },

      { path: '/forgot-password', component: () => import('pages/authentication/ForgotPasswordPage.vue') },
      { path: '/register-client', component: () => import('pages/RegistrationPage.vue') },
      { path: '/email-activation', component: () => import('pages/EmailActivationPage.vue') },
      { path: '/email-verified', component: () => import('pages/EmailVerifiedPage.vue') },
      
    ]
  },
  //Logged in user path (Can we get the user as std for logged in users?)
  {
    path: '/',
    component: () => import('layouts/UserLayout.vue'),
    children: [
      //Public Route
      { path: '/user', component: () => import('src/pages/LandingPage.vue') },
      
      
      { path: '/user/about', component: () => import('src/pages/AboutPage.vue') },
      // { path: '/scope', component: () => import('src/pages/ScopePage.vue')},
      { path: '/user/editorial-board', component: () => import('src/pages/EditorialPage.vue')},
      
      { path: '/user/open-access-policy', component: () => import('src/pages/OpenAccessPolicyPage.vue') },
      { path: '/user/copyright-policy', component: () => import('src/pages/CopyrightPage.vue') },
      { path: '/user/publication-ethics', component: () => import('src/pages/PublicationEthicsPage.vue') },
      { path: '/user/plagiarism-screening', component: () => import('src/pages/PlagiarismScreeningPage.vue') },
      { path: '/user/grammar-screening', component: () => import('src/pages/GrammarScreeningPage.vue') },      
      
      { path: '/user/contact', component: () => import('src/pages/ContactPage.vue') },
      { path: '/user/journals', component: () => import('src/pages/JournalPage.vue')},
      { path: '/user/:journalId/articles', component: () => import('src/pages/ArticleListPage.vue')},
      { path: '/user/:articleId/:articleTitle', component: () => import('src/pages/ArticlePage.vue')},
      
      { path: '/user/publish-article', component: () => import('src/pages/ArticlePublishPage.vue') },
      

      { path: '/user/subscription', component: () => import('src/pages/SubscriptionPage.vue')}
      
    ]
  },
  // Admin Path
  {
    path: '/',
    component: () => import('layouts/AdminLayout.vue'),
    children: [
      { path: '/admin/dashboard', component: () => import('pages/admin/DashboardPage.vue') },
      { path: '/admin/page-sections', component: () => import('pages/admin/PageSectionPage.vue') },
      { path: '/admin/users', component: () => import('pages/admin/UserPage.vue') },
      { path: '/admin/contacts', component: () => import('pages/admin/AdminContactPage.vue') },
      { path: '/admin/articles', component: () => import('src/pages/admin/AdminArticlesPage.vue') },
      { path: '/admin/articles/preview/:articleId', component: () => import('src/pages/admin/ArticleReviewPage.vue') },
      { path: '/admin/journals', component: () => import('src/pages/admin/JournalsPage.vue') },
      { path: '/admin/journals/reviewers', component: () => import('src/pages/admin/AdminJournalReviewersPage.vue') },
      { path: '/admin/journals/authors', component: () => import('src/pages/admin/AuthorsPage.vue') },
      
      { path: '/revise-article', component: () => import('src/pages/ArticleRevisionPage.vue') },
      { path: '/author-confirmation', component: () => import('src/pages/AuthorConfirmationPage.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes