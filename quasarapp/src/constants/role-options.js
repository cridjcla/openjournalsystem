export const ROLE_OPTIONS = [
    {
        label: 'Superadmin',
        value: 1
    },
    {
        label: 'Admin',
        value: 2
    },
    {
        label: 'User',
        value: 3
    },
    {
        label: 'Reviewer',
        value: 4
    },
];