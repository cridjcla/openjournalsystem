export const JOURNAL_STATUS_ENUM = {
    reviewing: 0,
    // first reviewer
    Admin_approved: 1,
    EIC_approved: 2,
    // if approved, eic will have access on selecting manegerial editor
    IPTBM_approved: 3,
    // selected managerial editor will send the link via email
    Peer_Reviewer_approved: 4,
    // managerial editor can publish/finalized
    Managing_Editor_approved: 5,
    // layout editor move the article to the template and will send as PDF
    Layout_Artist_approved: 6,
    // iptbm checked if the format 
    IPTBM_Proofreading: 7,
    // author confirmed that their article is ready to be published
    Author_confirmed: 8,
    // EIC will pubish
    Published: 9,
    // if article is rejected 
    Rejected: 10
}