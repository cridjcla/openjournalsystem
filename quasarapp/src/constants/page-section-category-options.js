export const PAGE_SECTION_CATEGORY_OPTIONS = [
    {
        label: 'Banner',
        value: 'banner',
        type: 'section'
    },
    {
        label: 'Achievement',
        value: 'achievement',
        type: 'section'
    },
    {
        label: 'Services',
        value: 'services',
        type: 'section'
    },
    {
        label: 'About the website',
        value: 'about',
        type: 'page'
    },
    {
        label: 'Editorial Board',
        value: 'editorial_board',
        type: 'page'
    },
    {
        label: 'Open Access Policy',
        value: 'open_access_policy',
        type: 'page'
    },
    {
        label: 'Copyright Policy',
        value: 'copyright_policy',
        type: 'page'
    },
    {
        label: 'Publication Ethics',
        value: 'publication_ethics',
        type: 'page'
    },
    {
        label: 'Plagiarism Screening',
        value: 'plagiarism_screening',
        type: 'page'
    },
    {
        label: 'Grammar Screening',
        value: 'grammar_screening',
        type: 'page'
    },
];