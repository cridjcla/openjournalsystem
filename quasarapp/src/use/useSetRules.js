export function useSetRules(val, dataType = 'string') {
  return val && (dataType == 'int' ? true : val.length > 0) || 'This field is required.';
}