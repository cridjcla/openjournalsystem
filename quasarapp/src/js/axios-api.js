import axios from "axios";

const BaseApi = axios.create({
  // baseURL: `${window.location.origin}/api/`,
  baseURL: process.env.DEV ? `http://localhost:8000/api/` : `${window.location.origin}/api/`,
});

const Api = () => {
  const token = localStorage.getItem("token");
  BaseApi.defaults.headers.common.Authorization = token
    ? `Bearer ${token}`
    : "";
  return BaseApi;
};

export default Api;