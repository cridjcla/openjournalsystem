import { defineStore } from 'pinia';
import api from 'src/js/api';
import { ref } from 'vue';

export const useStoreJournalCategories = defineStore('journalCategories', () =>{
  // private readonly
  const apiRoute = 'journal-categories'

  // state
  const journalCategories = ref([])

  // getters

  // actions
  const loadJournalCategories = () => {
    api.get(apiRoute)
        .then(response => {
          journalCategories.value = response.data
        })
        .catch(error => {
          console.error(error)
        })
  }

  // return
  return {
    // state
    journalCategories,

    // getters

    // actions
    loadJournalCategories
  }
})
