function setErrors(err) {
    return err?.response?.data?.errors;
}

function getErrorMessage(error) {
    return error ? error[0] : false;
}

function isError(error) {
    return error != undefined;
}

function setRules(val, dataType = null) {
    return val && (dataType == 'int' ? true : val.length > 0) || 'This field is required.';
}

function filterFn(val, origList) {
    if (!val || val == '') return origList;

    return origList.filter(
        (v) => typeof v == 'object' ? v.label.toLowerCase().indexOf(val.toLowerCase()) > -1 : v.toLowerCase().indexOf(val.toLowerCase()) > -1
    );
}

function formatMoney(number, decPlaces, decSep, thouSep) {
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSep = typeof decSep === "undefined" ? "." : decSep;
    thouSep = typeof thouSep === "undefined" ? "," : thouSep;
    var sign = number < 0 ? "-" : "";
    var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
    var j = (j = i.length) > 3 ? j % 3 : 0;

    return sign +
        (j ? i.substr(0, j) + thouSep : "") +
        i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
        (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

function getClientName(model) {
    if (!model || !model.id) return '';
    let fullname = model.corporation ? model.corporation : `${model.first_name} ${model.last_name}`
    return fullname;
}