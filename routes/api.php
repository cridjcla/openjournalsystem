<?php

use App\Http\Controllers\ArticleCommentController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\JournalController;
use App\Http\Controllers\PageSectionController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Unauthorized API */
#region landing
Route::prefix('page-sections')->group(function() {
    Route::get('', [PageSectionController::class, 'all']);
    Route::get('{category}', [PageSectionController::class, 'getFirstByCategory']);
});
#endregion landing

#region contacts
Route::post('contacts', [ContactController::class, 'create']);
#endregion landing

#region authentication
Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);

Route::get('verify-email', [AuthController::class, 'verifyEmail']);
Route::post('forgot-password', [AuthController::class, 'forgotPassword']);
#endregion authentication

#region journals
Route::prefix('journals/public')->group(function() {
    Route::get('', [JournalController::class, 'getPublicJournals']);
    Route::get('{id}', [JournalController::class, 'get']);
});

Route::prefix('articles/public')->group(function() {
    Route::get('', [ArticleController::class, 'getPublicArticlesByJournalId']);
    Route::get('peer-review/{uid}', [ArticleController::class, 'getByUid']);
    Route::get('comments/{uid}', [ArticleCommentController::class, 'getPublicArticleCommentsByUid']);
    Route::get('{id}', [ArticleController::class, 'get']);
    Route::post('status', [ArticleController::class, 'updatePublicStatusByUid']);
    Route::post('comment', [ArticleCommentController::class, 'storePublicCommentByArticleUId']);
});
#endregion

/** Authorized API */
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('user', function (Request $request) {
        return $request->user();
    });

    Route::get('authenticated', function () {
        return true;
    });

    // Users
    Route::prefix('users')->group(function() {
        Route::get('', [UserController::class, 'all']);
        Route::get('{id}', [UserController::class, 'get']);
        Route::post('', [UserController::class, 'create']);
        Route::post('update/role/{id}', [UserController::class, 'updateRoleById']);
        Route::post('{id?}', [UserController::class, 'createOrUpdate']);
        Route::delete('{id}', [UserController::class, 'delete']);
    });
    
    // User Authors
    Route::prefix('authors')->group(function() {
        Route::get('', [UserController::class, 'getAuthors']);
    });
    
    // Page Sections
    Route::prefix('page-sections')->group(function() {
        Route::get('{id}', [PageSectionController::class, 'get']);
        Route::post('{id?}', [PageSectionController::class, 'createOrUpdate']);
        Route::delete('{id}', [PageSectionController::class, 'delete']);
    });

    // Contacts
    Route::prefix('contacts')->group(function() {
        Route::get('/', [ContactController::class, 'all']);
        Route::get('{id}', [ContactController::class, 'get']);
        // Route::post('/', [ContactController::class, 'create']);
        Route::post('{id}', [ContactController::class, 'update']);
        Route::delete('{id}', [ContactController::class, 'remove']);
    });

    // Journals
    Route::prefix('journals')->group(function() {
        Route::get('', [JournalController::class, 'all']);
        Route::get('{id}', [JournalController::class, 'get']);
        Route::post('', [JournalController::class, 'create']);
        Route::post('status', [JournalController::class, 'updateStatusById']);
        Route::post('publish-status/{id}', [JournalController::class, 'updatePublishById']);
        Route::post('{id}', [JournalController::class, 'update']);
        Route::delete('{id}', [JournalController::class, 'delete']);
    });
    
    // Articles
    Route::prefix('articles')->group(function() {
        Route::get('admin', [ArticleController::class, 'all']);
        Route::get('admin/{id}', [ArticleController::class, 'get']);
        Route::post('/', [ArticleController::class, 'create']);
        Route::post('admin/upload-file', [ArticleController::class, 'updateFileById']);
        Route::post('admin/{id}', [ArticleController::class, 'update']);
        Route::post('admin/update/{id}', [ArticleController::class, 'updateColumn']);
        Route::post('status', [ArticleController::class, 'updateStatusById']);
        Route::post('managing-editor', [ArticleController::class, 'updateManagingEditorById']);
        Route::post('send-peer-review-email', [ArticleController::class, 'sendEmailToPeerReview']);
        Route::delete('{id}', [ArticleController::class, 'delete']);
    });
    
    Route::prefix('article-comments')->group(function() {
        Route::get('/', [ArticleCommentController::class, 'index']);
        Route::get('/article/{articleId}', [ArticleCommentController::class, 'getByArticleId']);
        Route::get('{id}', [ArticleCommentController::class, 'show']);
        Route::post('/', [ArticleCommentController::class, 'store']);
        Route::post('{id}', [ArticleCommentController::class, 'update']);
        Route::delete('{id}', [ArticleCommentController::class, 'destroy']);
    });
});